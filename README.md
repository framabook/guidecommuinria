Guide co-écrit par S. Ribas (Dir.), S. Ubeda, P. Guillaud.


**Titre :** Logiciels & objets libres.

**Sous-titre :** Animer une communauté autour d'un projet ouvert

La version *Markdown* est disponible en mode wiki sur [le wiki de ce projet](https://framagit.org/Framatophe/guidecommuinria/wikis/home).


Résumé
----------

Les communautés de pratique ont toujours été source d’innovation par le partage et l’entraide. En informatique, elles se sont révélées sous forme de clubs, de fanzines où la coopération prenait la forme d’échanges de listing ou de code sur supports électroniques. Internet à tout changé. Le développement coopératif est devenu mondial, les interactions entre communautés sont monnaies courantes, le partage s’est doublé d’un modèle économique tirant parti de ce monde ouvert. Cet écosystème est complexe mais extrêmement riche.

C’est de cela que traite cet ouvrage, fruit de l'expérience des chercheurs d’Inria travaillant sur des projets ouverts dans le domaine du numérique. Les concepts et les principes qu'il évoque s'adressent à toute personne cherchant à renforcer ses compétences dans le domaine de la gestion et d'animation de communauté et sont valables dans de multiples secteurs, qu'il s'agisse de logiciel, de matériel, de robotique, dans la recherche scientifique ou du côté des loisirs.

Cette aide méthodologique vous permettra de compartimenter vos actions en vue de maintenir une communauté de développement : animer, communiquer, publier le code, analyser le contexte de diffusion, savoir suivre et veiller à l'évolution du projet... Ce guide est accompagné d'interviews et de cas concrets qui, par leurs caractéristiques exemplaires, permettront au lecteur de trouver des réponses à des questions qu'il a probablement déjà rencontrées sans toujours savoir comment les traiter. 


À propos de Inria
---------------------


Institut national de recherche dédié au numérique, sous double tutelle des ministères en charge de la recherche et de l’industrie, \textbf{Inria} a pour missions de produire une recherche d’excellence dans les champs informatiques et mathématiques des sciences du numérique, et de garantir l’impact, notamment économique et sociétal, de cette recherche. Inria couvre l’ensemble du spectre des recherches au coeur de ces domaines d’activités, et intervient sur les questions, en lien avec le numérique, posées par les autres sciences et par les acteurs socioéconomiques.

Inria emploie 2700 collaborateurs issus des meilleures universités mondiales, qui relèvent les défis des sciences informatiques et mathématiques. Son modèle ouvert et agile lui permet d’explorer des voies originales avec ses partenaires industriels et académiques.


Les auteurs
-------------------

Stéphane Ribas, ingénieur en informatique et spécialiste de l’innovation, un parcours dans le monde de l’entreprise ; Patrick Guillaud, issu de l'industrie et créateur d'entreprises, docteur en management de la créativité appliqué à la conception ; Stéphane Ubeda, professeur en informatique à l’INSA de Lyon, créateur du laboratoire CITI et responsable d’une équipe Inria pendant plusieurs années ; tous les trois se retrouvent chez Inria au sein de la direction du développement technologique. À l’initiative de Stéphane Ribas, une méthodologie pour aider à l’animation de communautés dans des projets ouverts est mise au point, et quelques années plus tard, celle-ci devient le livre que vous avez dans les mains.
